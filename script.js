let dollarDisp = document.getElementsByClassName('dollar-num');
let strBar = document.getElementsByClassName('str-bar');
let strWidth = document.getElementsByClassName('str-width');
let strText = document.getElementsByClassName('str-text');
let intBar = document.getElementsByClassName('int-bar');
let intWidth = document.getElementsByClassName('int-width');
let intText = document.getElementsByClassName('int-text');
let chaBar = document.getElementsByClassName('cha-bar');
let chaWidth = document.getElementsByClassName('cha-width');
let chaText = document.getElementsByClassName('cha-text');
let strProgWidth = 0;
let intProgWidth = 0;
let chaProgWidth = 0;
let strMaxWidth = 20;
let intMaxWidth = 20;
let chaMaxWidth = 20;
let dollarNum = 0;
let strength = 0;
let intelligence = 0;
let charme = 0;
let strMax = 100;
let intMax = 100;
let chaMax = 100;
let gymPrice = 10;
let libPrice = 10;
let theaPrice = 10;
let coffeePrice = 30;
let bookPrice = 30;
let cosmeticPrice = 30;
let position = 1;
let schoolPosition = 0;
let petPosition = 1;
let gladLvl = 1;
let arenaOn = false;
let launchOn = false;
let scholarLvl = 0;
let brCourse1 = false;
let brCourse2 = false;
let brCourse3 = false;
let brCourse4 = false;
let siCourse1 = false;
let siCourse2 = false;
let siCourse3 = false;
let siCourse4 = false;
let goCourse1 = false;
let goCourse2 = false;
let goCourse3 = false;
let goCourse4 = false;
let politicValue = 0;
let politicMax = 1000;
let politicLvl = 1;
let love = 0;
let aPS = 0;
let distribPrice = 40;
let autoBrushPrice = 150;
let cookPrice = 1000;
let distribNum = 0;
let autoBrushNum = 0;
let cookNum = 0;
let distribIntervals = [];
let autoBrushIntervals = [];
let cookIntervals = [];
let distribSpeed = 100;
let autoBrushSpeed = 100;
let cookSpeed = 100;
let distribEff = 1;
let autoBrushEff = 3;
let cookEff = 15;
let distribAncPrice = 0;
let autoBrushAncPrice = 0;
let cookAncPrice = 0;
let feedOn = false;

function launch() {
	if (!launchOn) {
		$('#magasin').hide();
		$('#arene').hide();
		$('#school').hide();
		$('#school0').hide();
		$('#school1').hide();
		$('#school2').hide();
		$('#school3').hide();
		$('#return').hide();
		$('#politic').hide();
		$('#pet').hide();
		launchOn = true;
	}
}


// Work Function
function work() {
	dollarNum += 1;
	for (i = 0; i < dollarDisp.length; i++) {
		dollarDisp[i].innerHTML = `Vous disposez de ${Math.floor(dollarNum)} dollars`;
	}
}


// Workout Functions
function gym() {
	if (strength < strMax) {
		if (dollarNum >= gymPrice) {
			dollarNum -= gymPrice;
			for (i = 0; i < dollarDisp.length; i++) {
				dollarDisp[i].innerHTML = `Vous disposez de ${Math.floor(dollarNum)} dollars`;
			}
			gymPrice *= 1.3;
			document.getElementById('gymButton').setAttribute('title', `Cela vous coûtera ${Math.floor(gymPrice)} dollars`);
			strength += 10;
			strProgWidth += (10 * 100) / strMax;
			if (strProgWidth > 100) {
				strProgWidth = 100;
			}
			for (i = 0; i < strBar.length; i++) {
				strBar[i].style.width = `${strProgWidth}%`;
			}
			for (i = 0; i < strText.length; i++) {
				strText[i].innerHTML = `Force : ${Math.floor(strength)} / ${Math.floor(strMax)}`;
			}
			if (strength == strMax) {
				document.getElementById('gymButton').setAttribute('title', 'Vous êtes au maximum de votre force.');
			}
		}
	}
}

function library() {
	if (intelligence < intMax) {
		if (dollarNum >= libPrice) {
			dollarNum -= libPrice;
			for (i = 0; i < dollarDisp.length; i++) {
				dollarDisp[i].innerHTML = `Vous disposez de ${Math.floor(dollarNum)} dollars`;
			}
			libPrice *= 1.3;
			document.getElementById('libButton').setAttribute('title', `Cela vous coûtera ${Math.floor(libPrice)} dollars`);
			intelligence += 10;
			intProgWidth += (10 * 100) / intMax;
			if (intProgWidth > 100) {
				intProgWidth = 100;
			}
			for (i = 0; i < intBar.length; i++) {
				intBar[i].style.width = `${intProgWidth}%`;
			}
			for (i = 0; i < intText.length; i++) {
				intText[i].innerHTML = `Intelligence : ${Math.floor(intelligence)} / ${Math.floor(intMax)}`;
			}
			if (intelligence == intMax) {
				document.getElementById('libButton').setAttribute('title', 'Vous êtes au maximum de votre intelligence.');
			}
		}
	}
}

function theatre() {
	if (charme < chaMax) {
		if (dollarNum >= theaPrice) {
			dollarNum -= theaPrice;
			for (i = 0; i < dollarDisp.length; i++) {
				dollarDisp[i].innerHTML = `Vous disposez de ${Math.floor(dollarNum)} dollars`;
			}
			theaPrice *= 1.3;
			document.getElementById('theaButton').setAttribute('title', `Cela vous coûtera ${Math.floor(theaPrice)} dollars`);
			charme += 10;
			chaProgWidth += (10 * 100) / chaMax;
			if (chaProgWidth > 100) {
				chaProgWidth = 100;
			}
			for (i = 0; i < chaBar.length; i++) {
				chaBar[i].style.width = `${chaProgWidth}%`;
			}
			for (i = 0; i < chaText.length; i++) {
				chaText[i].innerHTML = `Charme : ${Math.floor(charme)} / ${Math.floor(chaMax)}`;
			}
			if (charme == chaMax) {
				document.getElementById('theaButton').setAttribute('title', 'Vous êtes au maximum de votre charme.');
			}
		}
	}
}
// End of Workout Functions


// Shop Functions
function coffee() {
	if (dollarNum >= coffeePrice) {
		if (strength == strMax) {
			document.getElementById('gymButton').setAttribute('title', `Cela vous coûtera ${Math.floor(gymPrice)} dollars`);
		}
		strMax += 10;
		strMaxWidth += 2;
		for (i = 0; i < strWidth.length; i++) {
			strWidth[i].style.width = `${strMaxWidth}%`;
		}
		strProgWidth = (strProgWidth * (strMax - 10)) / strMax;
		for (i = 0; i < strBar.length; i++) {
			strBar[i].style.width = `${strProgWidth}%`;
		}
		dollarNum -= coffeePrice;
		for (i = 0; i < dollarDisp.length; i++) {
			dollarDisp[i].innerHTML = `Vous disposez de ${Math.floor(dollarNum)} dollars`;
		}
		coffeePrice *= 1.5;
		document.getElementById('strMaxButton').setAttribute('title', `Cela vous coûtera ${Math.floor(coffeePrice)} dollars`);
		for (i = 0; i < strText.length; i++) {
			strText[i].innerHTML = `Force : ${Math.floor(strength)} / ${Math.floor(strMax)}`;
		}
	}
}

function book() {
	if (dollarNum >= bookPrice) {
		if (intelligence == intMax) {
			document.getElementById('libButton').setAttribute('title', `Cela vous coûtera ${Math.floor(libPrice)} dollars`);
		}
		intMax += 10;
		intMaxWidth += 2;
		for (i = 0; i < intWidth.length; i++) {
			intWidth[i].style.width = `${intMaxWidth}%`;
		}
		intProgWidth = (intProgWidth * (intMax - 10)) / intMax;
		for (i = 0; i < intBar.length; i++) {
			intBar[i].style.width = `${intProgWidth}%`;
		}
		dollarNum -= bookPrice;
		for (i = 0; i < dollarDisp.length; i++) {
			dollarDisp[i].innerHTML = `Vous disposez de ${Math.floor(dollarNum)} dollars`;
		}
		bookPrice *= 1.5;
		document.getElementById('intMaxButton').setAttribute('title', `Cela vous coûtera ${Math.floor(bookPrice)} dollars`);
		for (i = 0; i < intText.length; i++) {
			intText[i].innerHTML = `Intelligence : ${Math.floor(intelligence)} / ${Math.floor(intMax)}`;
		}
	}
}

function cosmetic() {
	if (dollarNum >= cosmeticPrice) {
		if (charme == chaMax) {
			document.getElementById('theaButton').setAttribute('title', `Cela vous coûtera ${Math.floor(theaPrice)} dollars`);
		}
		chaMax += 10;
		chaMaxWidth += 2;
		for (i = 0; i < chaWidth.length; i++) {
			chaWidth[i].style.width = `${chaMaxWidth}%`;
		}
		chaProgWidth = (chaProgWidth * (chaMax - 10)) / chaMax;
		for (i = 0; i < chaBar.length; i++) {
			chaBar[i].style.width = `${chaProgWidth}%`;
		}
		dollarNum -= cosmeticPrice;
		for (i = 0; i < dollarDisp.length; i++) {
			dollarDisp[i].innerHTML = `Vous disposez de ${Math.floor(dollarNum)} dollars`;
		}
		cosmeticPrice *= 1.5;
		document.getElementById('chaMaxButton').setAttribute('title', `Cela vous coûtera ${Math.floor(cosmeticPrice)} dollars`);
		for (i = 0; i < chaText.length; i++) {
			chaText[i].innerHTML = `Charme : ${Math.floor(charme)} / ${Math.floor(chaMax)}`;
		}
	}
}
// End of Shop Functions


// Arena Functions
function arena (lvl, exit) {
	if (!exit) {
		document.getElementById('areneButton').innerHTML = "Quitter l'Arene";
		switch (lvl) {
			case 1:
			if (strength <= 20) {
				document.getElementById('areneInfo').innerHTML = 'Vous êtes vaincu par le slime !';
				arenaOn = true;
			} else {
				document.getElementById('areneInfo').innerHTML = 'Vous avez vaincu le slime ! Votre niveau de gladiateur augmente de 1 !';
				gladLvl += 1;
				document.getElementById('areneLevel').innerHTML = `Votre niveau de gladiateur : ${gladLvl}`;
				arenaOn = true;
			}
			break;
			case 2:
			if (strength <= 30) {
				document.getElementById('areneInfo').innerHTML = 'Vous êtes vaincu par le squelette !';
				arenaOn = true;
			} else {
				document.getElementById('areneInfo').innerHTML = 'Vous avez vaincu le squelette ! Votre niveau de gladiateur augmente de 1 !';
				gladLvl += 1;
				document.getElementById('areneLevel').innerHTML = `Votre niveau de gladiateur : ${gladLvl}`;
				arenaOn = true;
			}
			break;
			case 3:
			if (strength <= 40) {
				document.getElementById('areneInfo').innerHTML = 'Vous êtes vaincu par le lézard !';
				arenaOn = true;
			} else {
				document.getElementById('areneInfo').innerHTML = 'Vous avez vaincu le lézard ! Votre niveau de gladiateur augmente de 1 !';
				gladLvl += 1;
				document.getElementById('areneLevel').innerHTML = `Votre niveau de gladiateur : ${gladLvl}`;
				arenaOn = true;
			}
			break;
			case 4:
			if (strength <= 50) {
				document.getElementById('areneInfo').innerHTML = 'Vous êtes vaincu par la chauve-souris géante !';
				arenaOn = true;
			} else {
				document.getElementById('areneInfo').innerHTML = 'Vous avez vaincu la chauve-souris géante ! Votre niveau de gladiateur augmente de 1 !';
				gladLvl += 1;
				document.getElementById('areneLevel').innerHTML = `Votre niveau de gladiateur : ${gladLvl}`;
				arenaOn = true;
			}
			break;
			case 5:
			if (strength <= 60) {
				document.getElementById('areneInfo').innerHTML = 'Vous êtes vaincu par le guerrier kobold !';
				arenaOn = true;
			} else {
				document.getElementById('areneInfo').innerHTML = 'Vous avez vaincu le guerrier kobold ! Votre niveau de gladiateur augmente de 1 !';
				gladLvl += 1;
				document.getElementById('areneLevel').innerHTML = `Votre niveau de gladiateur : ${gladLvl}`;
				arenaOn = true;
			}
			break;
			case 6:
			if (strength <= 70) {
				document.getElementById('areneInfo').innerHTML = 'Vous êtes vaincu par la liche !';
				arenaOn = true;
			} else {
				document.getElementById('areneInfo').innerHTML = 'Vous avez vaincu la liche ! Votre niveau de gladiateur augmente de 1 !';
				gladLvl += 1;
				document.getElementById('areneLevel').innerHTML = `Votre niveau de gladiateur : ${gladLvl}`;
				arenaOn = true;
			}
			break;
			case 7:
			if (strength <= 80) {
				document.getElementById('areneInfo').innerHTML = 'Vous êtes vaincu par le chef orc !';
				arenaOn = true;
			} else {
				document.getElementById('areneInfo').innerHTML = 'Vous avez vaincu le chef orc ! Votre niveau de gladiateur augmente de 1 !';
				gladLvl += 1;
				document.getElementById('areneLevel').innerHTML = `Votre niveau de gladiateur : ${gladLvl}`;
				arenaOn = true;
			}
			break;
			case 8:
			if (strength <= 90) {
				document.getElementById('areneInfo').innerHTML = 'Vous êtes vaincu par la mimique !';
				arenaOn = true;
			} else {
				document.getElementById('areneInfo').innerHTML = 'Vous avez vaincu la mimique ! Votre niveau de gladiateur augmente de 1 !';
				gladLvl += 1;
				document.getElementById('areneLevel').innerHTML = `Votre niveau de gladiateur : ${gladLvl}`;
				arenaOn = true;
			}
			break;
			case 9:
			if (strength <= 100) {
				document.getElementById('areneInfo').innerHTML = 'Vous êtes vaincu par le doppelganger !';
				arenaOn = true;
			} else {
				document.getElementById('areneInfo').innerHTML = 'Vous avez vaincu le doppelganger ! Votre niveau de gladiateur augmente de 1 !';
				gladLvl += 1;
				document.getElementById('areneLevel').innerHTML = `Votre niveau de gladiateur : ${gladLvl}`;
				arenaOn = true;
			}
			break;
			case 10:
			if (strength <= 120) {
				document.getElementById('areneInfo').innerHTML = 'Vous êtes vaincu par le dragon !';
				arenaOn = true;
			} else {
				document.getElementById('areneInfo').innerHTML = 'Vous avez vaincu le dragon ! Votre niveau de gladiateur augmente de 1 !';
				gladLvl += 1;
				document.getElementById('areneLevel').innerHTML = `Votre niveau de gladiateur : ${gladLvl}`;
				arenaOn = true;
			}
			break;
		}
	} else {
		switch (lvl) {
			case 1:
			document.getElementById('areneInfo').innerHTML = 'Vous allez combattre un slime, il dispose de 20 de force.';
			document.getElementById('areneButton').innerHTML = 'Combattre';
			arenaOn = false;
			break;
			case 2:
			document.getElementById('areneInfo').innerHTML = 'Vous allez combattre un squelette, il dispose de 30 de force.';
			document.getElementById('areneButton').innerHTML = 'Combattre';
			arenaOn = false;
			break;
			case 3:
			document.getElementById('areneInfo').innerHTML = 'Vous allez combattre un lézard, il dispose de 40 de force.';
			document.getElementById('areneButton').innerHTML = 'Combattre';
			arenaOn = false;
			break;
			case 4:
			document.getElementById('areneInfo').innerHTML = 'Vous allez combattre une chauve-souris géante, elle dispose de 50 de force.';
			document.getElementById('areneButton').innerHTML = 'Combattre';
			arenaOn = false;
			break;
			case 5:
			document.getElementById('areneInfo').innerHTML = 'Vous allez combattre un guerrier kobold, il dispose de 60 de force.';
			document.getElementById('areneButton').innerHTML = 'Combattre';
			arenaOn = false;
			break;
			case 6:
			document.getElementById('areneInfo').innerHTML = 'Vous allez combattre une liche, elle dispose de 70 de force.';
			document.getElementById('areneButton').innerHTML = 'Combattre';
			arenaOn = false;
			break;
			case 7:
			document.getElementById('areneInfo').innerHTML = 'Vous allez combattre un chef orc, il dispose de 80 de force.';
			document.getElementById('areneButton').innerHTML = 'Combattre';
			arenaOn = false;
			break;
			case 8:
			document.getElementById('areneInfo').innerHTML = 'Vous allez combattre une mimique, elle dispose de 90 de force.';
			document.getElementById('areneButton').innerHTML = 'Combattre';
			arenaOn = false;
			break;
			case 9:
			document.getElementById('areneInfo').innerHTML = 'Vous allez combattre un doppelganger, il dispose de 100 de force.';
			document.getElementById('areneButton').innerHTML = 'Combattre';
			arenaOn = false;
			break;
			case 10:
			document.getElementById('areneInfo').innerHTML = 'Vous allez combattre un dragon, il dispose de 120 de force.';
			document.getElementById('areneButton').innerHTML = 'Combattre';
			arenaOn = false;
			break;
			case 11:
			document.getElementById('areneInfo').innerHTML = "Vous êtes le champion d'arene !";
			document.getElementById('areneButton').innerHTML = 'Closed'
			break;
		}
	}
}
// End of Arena Functions


// School Functions
// School Movement Functions
function bronze() {
	$('#school0').hide();
	$('#school1').show();
	$('#return').show();
	schoolPosition = 1;
}

function silver() {
	$('#school0').hide();
	$('#school2').show();
	$('#return').show();
	schoolPosition = 2;
}

function gold() {
	$('#school0').hide();
	$('#school3').show();
	$('#return').show();
	schoolPosition = 3;
}

function schoolBack(schPos) {
	switch (schPos) {
		case 1:
		$('#school1').hide();
		$('#return').hide();
		$('#school0').show();
		schoolPosition = 0;
		break;
		case 2:
		$('#school2').hide();
		$('#return').hide();
		$('#school0').show();
		schoolPosition = 0;
		break;
		case 3:
		$('#school3').hide();
		$('#return').hide();
		$('#school0').show();
		schoolPosition = 0;
		break;
	}
}
// End of School Movement Functions

// Scholar Level Function
let scholarTitle = document.getElementsByClassName('scholar-lvl');


function scholarLvlCalc (scholarLvl) {
	switch (scholarLvl) {
		case 0:
		for (i = 0; i < scholarTitle.length; i++) {
			scholarTitle[i].innerHTML = 'Votre niveau scolaire : Inculte';
		}
		break;

		case 1:
		for (i = 0; i < scholarTitle.length; i++) {
			scholarTitle[i].innerHTML = 'Votre niveau scolaire : Collège';
		}
		break;

		case 2:
		for (i = 0; i < scholarTitle.length; i++) {
			scholarTitle[i].innerHTML = 'Votre niveau scolaire : Lycée';
		}
		break;

		case 3:
		for (i = 0; i < scholarTitle.length; i++) {
			scholarTitle[i].innerHTML = 'Votre niveau scolaire : Universitée';
		}
		break;
	}
}
// End of Scholar Level Function

// School Courses Functions
let schoolInfos = document.getElementsByClassName('school-infos');

function brCourse1Func() {
	if (!brCourse1) {
		if (intelligence < 20) {
			for (i = 0; i < schoolInfos.length; i++) {
				schoolInfos[i].innerHTML = "Vous manquez d'intelligence pour passer le test.";
				setTimeout(function(){for (i = 0; i < schoolInfos.length; i++) {
					schoolInfos[i].innerHTML = 'Choisissez votre cours.';
				}}, 2000)
			}
		} else {
			for (i = 0; i < schoolInfos.length; i++) {
				schoolInfos[i].innerHTML = 'Bravo ! Vous avez réussi le test !';
				setTimeout(function(){for (i = 0; i < schoolInfos.length; i++) {
					schoolInfos[i].innerHTML = 'Choisissez votre cours.';
				}}, 2000)
			}
			brCourse1 = true;
			document.getElementById('brCourse1').style.borderColor = 'green';
			document.getElementById('brCourse1').style.color = 'green';
			document.getElementById('brCourse1').onmouseover = function() {
				this.style.backgroundColor = 'black';
			};
			if (brCourse1 && brCourse2 && brCourse3 && brCourse4) {
				scholarLvl++;
				scholarLvlCalc(scholarLvl);
			}
		}
	} else {
		for (i = 0; i < schoolInfos.length; i++) {
			schoolInfos[i].innerHTML = 'Vous avez déjà passé ce cours.';
			setTimeout(function(){for (i = 0; i < schoolInfos.length; i++) {
				schoolInfos[i].innerHTML = 'Choisissez votre cours.';
			}}, 2000)
		}
	}
}

function brCourse2Func() {
	if (!brCourse2) {
		if (intelligence < 30) {
			for (i = 0; i < schoolInfos.length; i++) {
				schoolInfos[i].innerHTML = "Vous manquez d'intelligence pour passer le test.";
				setTimeout(function(){for (i = 0; i < schoolInfos.length; i++) {
					schoolInfos[i].innerHTML = 'Choisissez votre cours.';
				}}, 2000)
			}
		} else {
			for (i = 0; i < schoolInfos.length; i++) {
				schoolInfos[i].innerHTML = 'Bravo ! Vous avez réussi le test !';
				setTimeout(function(){for (i = 0; i < schoolInfos.length; i++) {
					schoolInfos[i].innerHTML = 'Choisissez votre cours.';
				}}, 2000)
			}
			brCourse2 = true;
			document.getElementById('brCourse2').style.borderColor = 'green';
			document.getElementById('brCourse2').style.color = 'green';
			document.getElementById('brCourse2').onmouseover = function() {
				this.style.backgroundColor = 'black';
			};
			if (brCourse1 && brCourse2 && brCourse3 && brCourse4) {
				scholarLvl++;
				scholarLvlCalc(scholarLvl);
			}
		}
	} else {
		for (i = 0; i < schoolInfos.length; i++) {
			schoolInfos[i].innerHTML = 'Vous avez déjà passé ce cours.';
			setTimeout(function(){for (i = 0; i < schoolInfos.length; i++) {
				schoolInfos[i].innerHTML = 'Choisissez votre cours.';
			}}, 2000)
		}
	}
}

function brCourse3Func() {
	if (!brCourse3) {
		if (intelligence < 40) {
			for (i = 0; i < schoolInfos.length; i++) {
				schoolInfos[i].innerHTML = "Vous manquez d'intelligence pour passer le test.";
				setTimeout(function(){for (i = 0; i < schoolInfos.length; i++) {
					schoolInfos[i].innerHTML = 'Choisissez votre cours.';
				}}, 2000)
			}
		} else {
			for (i = 0; i < schoolInfos.length; i++) {
				schoolInfos[i].innerHTML = 'Bravo ! Vous avez réussi le test !';
				setTimeout(function(){for (i = 0; i < schoolInfos.length; i++) {
					schoolInfos[i].innerHTML = 'Choisissez votre cours.';
				}}, 2000)
			}
			brCourse3 = true;
			document.getElementById('brCourse3').style.borderColor = 'green';
			document.getElementById('brCourse3').style.color = 'green';
			document.getElementById('brCourse3').onmouseover = function() {
				this.style.backgroundColor = 'black';
			};
			if (brCourse1 && brCourse2 && brCourse3 && brCourse4) {
				scholarLvl++;
				scholarLvlCalc(scholarLvl);
			}
		}
	} else {
		for (i = 0; i < schoolInfos.length; i++) {
			schoolInfos[i].innerHTML = 'Vous avez déjà passé ce cours.';
			setTimeout(function(){for (i = 0; i < schoolInfos.length; i++) {
				schoolInfos[i].innerHTML = 'Choisissez votre cours.';
			}}, 2000)
		}
	}
}

function brCourse4Func() {
	if (!brCourse4) {
		if (intelligence < 50) {
			for (i = 0; i < schoolInfos.length; i++) {
				schoolInfos[i].innerHTML = "Vous manquez d'intelligence pour passer le test.";
				setTimeout(function(){for (i = 0; i < schoolInfos.length; i++) {
					schoolInfos[i].innerHTML = 'Choisissez votre cours.';
				}}, 2000)
			}
		} else {
			for (i = 0; i < schoolInfos.length; i++) {
				schoolInfos[i].innerHTML = 'Bravo ! Vous avez réussi le test !';
				setTimeout(function(){for (i = 0; i < schoolInfos.length; i++) {
					schoolInfos[i].innerHTML = 'Choisissez votre cours.';
				}}, 2000)
			}
			brCourse4 = true;
			document.getElementById('brCourse4').style.borderColor = 'green';
			document.getElementById('brCourse4').style.color = 'green';
			document.getElementById('brCourse4').onmouseover = function() {
				this.style.backgroundColor = 'black';
			};
			if (brCourse1 && brCourse2 && brCourse3 && brCourse4) {
				scholarLvl++;
				scholarLvlCalc(scholarLvl);
			}
		}
	} else {
		for (i = 0; i < schoolInfos.length; i++) {
			schoolInfos[i].innerHTML = 'Vous avez déjà passé ce cours.';
			setTimeout(function(){for (i = 0; i < schoolInfos.length; i++) {
				schoolInfos[i].innerHTML = 'Choisissez votre cours.';
			}}, 2000)
		}
	}
}

function siCourse1Func() {
	if (!siCourse1) {
		if (intelligence < 60) {
			for (i = 0; i < schoolInfos.length; i++) {
				schoolInfos[i].innerHTML = "Vous manquez d'intelligence pour passer le test.";
				setTimeout(function(){for (i = 0; i < schoolInfos.length; i++) {
					schoolInfos[i].innerHTML = 'Choisissez votre cours.';
				}}, 2000)
			}
		} else {
			for (i = 0; i < schoolInfos.length; i++) {
				schoolInfos[i].innerHTML = 'Bravo ! Vous avez réussi le test !';
				setTimeout(function(){for (i = 0; i < schoolInfos.length; i++) {
					schoolInfos[i].innerHTML = 'Choisissez votre cours.';
				}}, 2000)
			}
			siCourse1 = true;
			document.getElementById('siCourse1').style.borderColor = 'green';
			document.getElementById('siCourse1').style.color = 'green';
			document.getElementById('siCourse1').onmouseover = function() {
				this.style.backgroundColor = 'black';
			};
			if (siCourse1 && siCourse2 && siCourse3 && siCourse4) {
				scholarLvl++;
				scholarLvlCalc(scholarLvl);
			}
		}
	} else {
		for (i = 0; i < schoolInfos.length; i++) {
			schoolInfos[i].innerHTML = 'Vous avez déjà passé ce cours.';
			setTimeout(function(){for (i = 0; i < schoolInfos.length; i++) {
				schoolInfos[i].innerHTML = 'Choisissez votre cours.';
			}}, 2000)
		}
	}
}

function siCourse2Func() {
	if (!siCourse2) {
		if (intelligence < 70) {
			for (i = 0; i < schoolInfos.length; i++) {
				schoolInfos[i].innerHTML = "Vous manquez d'intelligence pour passer le test.";
				setTimeout(function(){for (i = 0; i < schoolInfos.length; i++) {
					schoolInfos[i].innerHTML = 'Choisissez votre cours.';
				}}, 2000)
			}
		} else {
			for (i = 0; i < schoolInfos.length; i++) {
				schoolInfos[i].innerHTML = 'Bravo ! Vous avez réussi le test !';
				setTimeout(function(){for (i = 0; i < schoolInfos.length; i++) {
					schoolInfos[i].innerHTML = 'Choisissez votre cours.';
				}}, 2000)
			}
			siCourse2 = true;
			document.getElementById('siCourse2').style.borderColor = 'green';
			document.getElementById('siCourse2').style.color = 'green';
			document.getElementById('siCourse2').onmouseover = function() {
				this.style.backgroundColor = 'black';
			};
			if (siCourse1 && siCourse2 && siCourse3 && siCourse4) {
				scholarLvl++;
				scholarLvlCalc(scholarLvl);
			}
		}
	} else {
		for (i = 0; i < schoolInfos.length; i++) {
			schoolInfos[i].innerHTML = 'Vous avez déjà passé ce cours.';
			setTimeout(function(){for (i = 0; i < schoolInfos.length; i++) {
				schoolInfos[i].innerHTML = 'Choisissez votre cours.';
			}}, 2000)
		}
	}
}

function siCourse3Func() {
	if (!siCourse3) {
		if (intelligence < 80) {
			for (i = 0; i < schoolInfos.length; i++) {
				schoolInfos[i].innerHTML = "Vous manquez d'intelligence pour passer le test.";
				setTimeout(function(){for (i = 0; i < schoolInfos.length; i++) {
					schoolInfos[i].innerHTML = 'Choisissez votre cours.';
				}}, 2000)
			}
		} else {
			for (i = 0; i < schoolInfos.length; i++) {
				schoolInfos[i].innerHTML = 'Bravo ! Vous avez réussi le test !';
				setTimeout(function(){for (i = 0; i < schoolInfos.length; i++) {
					schoolInfos[i].innerHTML = 'Choisissez votre cours.';
				}}, 2000)
			}
			siCourse3 = true;
			document.getElementById('siCourse3').style.borderColor = 'green';
			document.getElementById('siCourse3').style.color = 'green';
			document.getElementById('siCourse3').onmouseover = function() {
				this.style.backgroundColor = 'black';
			};
			if (siCourse1 && siCourse2 && siCourse3 && siCourse4) {
				scholarLvl++;
				scholarLvlCalc(scholarLvl);
			}
		}
	} else {
		for (i = 0; i < schoolInfos.length; i++) {
			schoolInfos[i].innerHTML = 'Vous avez déjà passé ce cours.';
			setTimeout(function(){for (i = 0; i < schoolInfos.length; i++) {
				schoolInfos[i].innerHTML = 'Choisissez votre cours.';
			}}, 2000)
		}
	}
}

function siCourse4Func() {
	if (!siCourse4) {
		if (intelligence < 90) {
			for (i = 0; i < schoolInfos.length; i++) {
				schoolInfos[i].innerHTML = "Vous manquez d'intelligence pour passer le test.";
				setTimeout(function(){for (i = 0; i < schoolInfos.length; i++) {
					schoolInfos[i].innerHTML = 'Choisissez votre cours.';
				}}, 2000)
			}
		} else {
			for (i = 0; i < schoolInfos.length; i++) {
				schoolInfos[i].innerHTML = 'Bravo ! Vous avez réussi le test !';
				setTimeout(function(){for (i = 0; i < schoolInfos.length; i++) {
					schoolInfos[i].innerHTML = 'Choisissez votre cours.';
				}}, 2000)
			}
			siCourse4 = true;
			document.getElementById('siCourse4').style.borderColor = 'green';
			document.getElementById('siCourse4').style.color = 'green';
			document.getElementById('siCourse4').onmouseover = function() {
				this.style.backgroundColor = 'black';
			};
			if (siCourse1 && siCourse2 && siCourse3 && siCourse4) {
				scholarLvl++;
				scholarLvlCalc(scholarLvl);
			}
		}
	} else {
		for (i = 0; i < schoolInfos.length; i++) {
			schoolInfos[i].innerHTML = 'Vous avez déjà passé ce cours.';
			setTimeout(function(){for (i = 0; i < schoolInfos.length; i++) {
				schoolInfos[i].innerHTML = 'Choisissez votre cours.';
			}}, 2000)
		}
	}
}

function goCourse1Func() {
	if (!goCourse1) {
		if (intelligence < 100) {
			for (i = 0; i < schoolInfos.length; i++) {
				schoolInfos[i].innerHTML = "Vous manquez d'intelligence pour passer le test.";
				setTimeout(function(){for (i = 0; i < schoolInfos.length; i++) {
					schoolInfos[i].innerHTML = 'Choisissez votre cours.';
				}}, 2000)
			}
		} else {
			for (i = 0; i < schoolInfos.length; i++) {
				schoolInfos[i].innerHTML = 'Bravo ! Vous avez réussi le test !';
				setTimeout(function(){for (i = 0; i < schoolInfos.length; i++) {
					schoolInfos[i].innerHTML = 'Choisissez votre cours.';
				}}, 2000)
			}
			goCourse1 = true;
			document.getElementById('goCourse1').style.borderColor = 'green';
			document.getElementById('goCourse1').style.color = 'green';
			document.getElementById('goCourse1').onmouseover = function() {
				this.style.backgroundColor = 'black';
			};
			if (goCourse1 && goCourse2 && goCourse3 && goCourse4) {
				scholarLvl++;
				scholarLvlCalc(scholarLvl);
			}
		}
	} else {
		for (i = 0; i < schoolInfos.length; i++) {
			schoolInfos[i].innerHTML = 'Vous avez déjà passé ce cours.';
			setTimeout(function(){for (i = 0; i < schoolInfos.length; i++) {
				schoolInfos[i].innerHTML = 'Choisissez votre cours.';
			}}, 2000)
		}
	}
}

function goCourse2Func() {
	if (!goCourse2) {
		if (intelligence < 120) {
			for (i = 0; i < schoolInfos.length; i++) {
				schoolInfos[i].innerHTML = "Vous manquez d'intelligence pour passer le test.";
				setTimeout(function(){for (i = 0; i < schoolInfos.length; i++) {
					schoolInfos[i].innerHTML = 'Choisissez votre cours.';
				}}, 2000)
			}
		} else {
			for (i = 0; i < schoolInfos.length; i++) {
				schoolInfos[i].innerHTML = 'Bravo ! Vous avez réussi le test !';
				setTimeout(function(){for (i = 0; i < schoolInfos.length; i++) {
					schoolInfos[i].innerHTML = 'Choisissez votre cours.';
				}}, 2000)
			}
			goCourse2 = true;
			document.getElementById('goCourse2').style.borderColor = 'green';
			document.getElementById('goCourse2').style.color = 'green';
			document.getElementById('goCourse2').onmouseover = function() {
				this.style.backgroundColor = 'black';
			};
			if (goCourse1 && goCourse2 && goCourse3 && goCourse4) {
				scholarLvl++;
				scholarLvlCalc(scholarLvl);
			}
		}
	} else {
		for (i = 0; i < schoolInfos.length; i++) {
			schoolInfos[i].innerHTML = 'Vous avez déjà passé ce cours.';
			setTimeout(function(){for (i = 0; i < schoolInfos.length; i++) {
				schoolInfos[i].innerHTML = 'Choisissez votre cours.';
			}}, 2000)
		}
	}
}

function goCourse3Func() {
	if (!goCourse3) {
		if (intelligence < 140) {
			for (i = 0; i < schoolInfos.length; i++) {
				schoolInfos[i].innerHTML = "Vous manquez d'intelligence pour passer le test.";
				setTimeout(function(){for (i = 0; i < schoolInfos.length; i++) {
					schoolInfos[i].innerHTML = 'Choisissez votre cours.';
				}}, 2000)
			}
		} else {
			for (i = 0; i < schoolInfos.length; i++) {
				schoolInfos[i].innerHTML = 'Bravo ! Vous avez réussi le test !';
				setTimeout(function(){for (i = 0; i < schoolInfos.length; i++) {
					schoolInfos[i].innerHTML = 'Choisissez votre cours.';
				}}, 2000)
			}
			goCourse3 = true;
			document.getElementById('goCourse3').style.borderColor = 'green';
			document.getElementById('goCourse3').style.color = 'green';
			document.getElementById('goCourse3').onmouseover = function() {
				this.style.backgroundColor = 'black';
			};
			if (goCourse1 && goCourse2 && goCourse3 && goCourse4) {
				scholarLvl++;
				scholarLvlCalc(scholarLvl);
			}
		}
	} else {
		for (i = 0; i < schoolInfos.length; i++) {
			schoolInfos[i].innerHTML = 'Vous avez déjà passé ce cours.';
			setTimeout(function(){for (i = 0; i < schoolInfos.length; i++) {
				schoolInfos[i].innerHTML = 'Choisissez votre cours.';
			}}, 2000)
		}
	}
}

function goCourse4Func() {
	if (!goCourse4) {
		if (intelligence < 150) {
			for (i = 0; i < schoolInfos.length; i++) {
				schoolInfos[i].innerHTML = "Vous manquez d'intelligence pour passer le test.";
				setTimeout(function(){for (i = 0; i < schoolInfos.length; i++) {
					schoolInfos[i].innerHTML = 'Choisissez votre cours.';
				}}, 2000)
			}
		} else {
			for (i = 0; i < schoolInfos.length; i++) {
				schoolInfos[i].innerHTML = 'Bravo ! Vous avez réussi le test !';
				setTimeout(function(){for (i = 0; i < schoolInfos.length; i++) {
					schoolInfos[i].innerHTML = 'Choisissez votre cours.';
				}}, 2000)
			}
			goCourse4 = true;
			document.getElementById('goCourse1').style.borderColor = 'green';
			document.getElementById('goCourse1').style.color = 'green';
			document.getElementById('goCourse1').onmouseover = function() {
				this.style.backgroundColor = 'black';
			};
			if (goCourse1 && goCourse2 && goCourse3 && goCourse4) {
				scholarLvl++;
				scholarLvlCalc(scholarLvl);
			}
		}
	} else {
		for (i = 0; i < schoolInfos.length; i++) {
			schoolInfos[i].innerHTML = 'Vous avez déjà passé ce cours.';
			setTimeout(function(){for (i = 0; i < schoolInfos.length; i++) {
				schoolInfos[i].innerHTML = 'Choisissez votre cours.';
			}}, 2000)
		}
	}
}
// End of School Courses Functions
// End of School Function


// Politic Functions
let politicBarAdv = 10;
let politicBarCurAdv = 0;

function politicLvlUp(lvl) {
	switch (lvl) {
		case 2:
		document.getElementById('politicLvl').innerHTML = 'Votre niveau politique : Amateur';
		politicValue = 0;
		politicBarAdv = 30;
		break;

		case 3:
		document.getElementById('politicLvl').innerHTML = 'Votre niveau politique : Initié';
		politicValue = 0;
		politicBarAdv = 60;
		break;

		case 4:
		document.getElementById('politicLvl').innerHTML = 'Votre niveau politique : Maire';
		politicValue = 0;
		politicBarAdv = 120;
		break;

		case 5:
		document.getElementById('politicLvl').innerHTML = 'Votre niveau politique : Ministre';
		politicValue = 0;
		politicBarAdv = 240;
		break;

		case 6:
		document.getElementById('politicLvl').innerHTML = 'Votre niveau politique : Président';
		politicValue = 0;
		politicMax = 0;
		$('#camp-bar-parent').hide();
		$('#camp-bar').hide();
		$('#politicButton').hide();
		document.getElementById('president').innerHTML = 'Vous êtes à la fin de votre carrière politique.'
		break;
	}
}

function campagne() {
	politicBarCurAdv = charme / politicBarAdv;
	politicValue += politicBarCurAdv;
	document.getElementById('camp-bar').style.width = `${politicValue}%`;
	if (politicValue >= 100) {
		politicLvl++;
		politicLvlUp(politicLvl);
	}
}
// End of Politic Functions

// Pet Functions
// Launch & Choice
function petLaunch(petPos) {
	switch (petPos) {
		case 1:
		for (i = 0; i < dollarDisp.length; i++) {
			dollarDisp[i].innerHTML = `Vous disposez de ${Math.floor(dollarNum)} dollars`;
		}
		$('#pet').show();
		$('#upg1').hide();
		$('#upg2').hide();
		$('#upg3').hide();
		$('#pet1').show();
		$('#pet2').hide();
		$('#pet3').hide();
		break;

		case 2:
		for (i = 0; i < dollarDisp.length; i++) {
			dollarDisp[i].innerHTML = `Vous disposez de ${Math.floor(dollarNum)} dollars`;
		}
		$('#pet').show();
		break;

		case 3:
		$('#pet').show();
		break;
	}
}

function petBuy() {
	$('#pet1').hide();
	$('#pet2').show();
	petPosition = 2;
}

function animalChoice (choice) {
	if (dollarNum >= 100) {
		dollarNum -= 100;
		for (i = 0; i < dollarDisp.length; i++) {
			dollarDisp[i].innerHTML = `Vous disposez de ${Math.floor(dollarNum)} dollars`;
		}
		switch (choice) {
			case 1:
			document.getElementById('pet-choice').innerHTML = "Vous disposez d'un chat";
			break;

			case 2:
			document.getElementById('pet-choice').innerHTML = "Vous disposez d'un chien";
			break;
		}
		$('#pet2').hide();
		$('#pet3').show();
		$('#cook').hide();
		$('#distrib').hide();
		$('#auto-brush').hide();
		$('#distrib-sell').hide();
		$('#auto-brush-sell').hide();
		$('#cook-sell').hide();
		$('#distrib-num').hide();
		$('#auto-brush-num').hide();
		$('#cook-num').hide();
		$('#upg1').hide();
		$('#upg2').hide();
		$('#upg3').hide();
		petPosition = 3;	
	}
	
}
// End of Launch & Choice

// Love Generation
upg1bol = false;
upg2bol = false;
upg3bol = false;

function feed() {
	love += 50;
	document.getElementById('love').innerHTML = `Vous avez ${Math.floor(love)} d'affection`;
	if (love >= 20) {
		document.getElementById('distrib').setAttribute('title', `Cela vous coûtera ${Math.floor(distribPrice)} d'affection. Cela rapporte ${Math.round(distribEff*100)/100} d'affection par seconde.`);
		$('#distrib').show();
	}
	if (love >= 100) {
		document.getElementById('auto-brush').setAttribute('title', `Cela vous coûtera ${Math.floor(autoBrushPrice)} d'affection. Cela rapporte ${Math.round(autoBrushEff*100)/100} d'affection par seconde.`);
		$('#auto-brush').show();
	}
	if (love >= 150 && distribNum >= 1 && !upg1bol) {
		$('#upg1').show();
		upg1bol = true;
	}
	if (love >= 500) {
		if (autoBrushNum >= 1 && !upg2bol) {
			$('#upg2').show();
			upg2bol = true;
		}
		document.getElementById('cook').setAttribute('title', `Cela vous coûtera ${Math.floor(cookPrice)} d'affection. Cela rapporte ${Math.round(cookEff*100)/100} d'affection par seconde.`);
		$('#cook').show();
	}
	if (love >= 1500 && cookNum >= 1 && !upg3bol) {
		$('#upg3').show();
		upg3bol = true;
	}
}

function distribGen() {
	love += 0.1;
	document.getElementById('love').innerHTML = `Vous avez ${Math.floor(love)} d'affection`;
	if (love >= 100) {
		document.getElementById('auto-brush').setAttribute('title', `Cela vous coûtera ${Math.floor(autoBrushPrice)} d'affection. Cela rapporte ${Math.round(autoBrushEff*100)/100} d'affection par seconde.`);
		$('#auto-brush').show();
	}
	if (love >= 150 && distribNum >= 1 && !upg1bol) {
		$('#upg1').show();
		upg1bol = true;
	}
	if (love >= 500) {
		document.getElementById('cook').setAttribute('title', `Cela vous coûtera ${Math.floor(cookPrice)} d'affection. Cela rapporte ${Math.round(cookEff*100)/100} d'affection par seconde.`);
		$('#cook').show();
		if (autoBrushNum >= 1 && !upg2bol) {
			$('#upg2').show();
			upg2bol = true;
		}
	}
	if (love >= 1500 && cookNum >= 1 && !upg3bol) {
		$('#upg3').show();
		upg3bol = true;
	}
}

function autoBrushGen() {
	love += 0.3;
	document.getElementById('love').innerHTML = `Vous avez ${Math.floor(love)} d'affection`;
	if (love >= 150 && distribNum >= 1 && !upg1bol) {
		$('#upg1').show();
		upg1bol = true;
	}
	if (love >= 500) {
		document.getElementById('cook').setAttribute('title', `Cela vous coûtera ${Math.floor(cookPrice)} d'affection. Cela rapporte ${Math.round(cookEff*100)/100} d'affection par seconde.`);
		$('#cook').show();
		if (autoBrushNum >= 1 && !upg2bol) {
			$('#upg2').show();
			upg2bol = true;
		}
	}
	if (love >= 1500 && cookNum >= 1 && !upg3bol) {
		$('#upg3').show();
		upg3bol = true;
	}
}

function cookGen() {
	love += 1.5;
	document.getElementById('love').innerHTML = `Vous avez ${Math.floor(love)} d'affection`;
	if (love >= 1500 && cookNum >= 1 && !upg3bol) {
		$('#upg3').show();
		upg3bol = true;
	}
}
// End of Love Generation

// Buy Functions
function distrib() {
	if (love >= distribPrice) {
		love -= distribPrice;
		document.getElementById('love').innerHTML = `Vous avez ${Math.floor(love)} d'affection`;
		distribAncPrice = distribPrice / 2;
		distribPrice *= 1.5;
		document.getElementById('distrib').setAttribute('title', `Cela vous coûtera ${Math.floor(distribPrice)} d'affection. Cela rapporte ${Math.round(distribEff*100)/100} d'affection par seconde.`);
		distribNum++;
		document.getElementById('distrib-num').innerHTML = `Vous disposez de ${distribNum} Distributeurs Automatique.`;
		document.getElementById('distrib-sell').setAttribute('title', `Cela vous rapportera ${Math.floor(distribAncPrice)} d'affection.`);
		document.getElementById('distrib-sell').style.borderColor = '#ffc107';
		document.getElementById('distrib-sell').style.color = '#ffc107';
		document.getElementById('distrib-sell').onmouseover = function() {
			document.getElementById('distrib-sell').style.backgroundColor = '#ffc107';
			document.getElementById('distrib-sell').style.color = '#212529';
		};
		document.getElementById('distrib-sell').onmouseout = function() {
			document.getElementById('distrib-sell').style.backgroundColor = 'black';
			document.getElementById('distrib-sell').style.color = '#ffc107';
		};
		aPS += distribEff;
		document.getElementById('aPS').innerHTML = `Affection Par Seconde : ${Math.round(aPS*100)/100}`;
		$('#distrib-sell').show();
		$('#distrib-num').show();
		distribIntervals.push(window.setInterval(distribGen, distribSpeed));
	}
}

function autoBrush() {
	if (love >= autoBrushPrice) {
		love -= autoBrushPrice;
		document.getElementById('love').innerHTML = `Vous avez ${Math.floor(love)} d'affection`;
		autoBrushAncPrice = autoBrushPrice / 2;
		autoBrushPrice *= 1.6;
		document.getElementById('auto-brush').setAttribute('title', `Cela vous coûtera ${Math.floor(autoBrushPrice)} d'affection. Cela rapporte ${Math.round(autoBrushEff*100)/100} d'affection par seconde.`);
		autoBrushNum++;
		document.getElementById('auto-brush-num').innerHTML = `Vous disposez de ${autoBrushNum} Brosses Automatiques.`;
		document.getElementById('auto-brush-sell').setAttribute('title', `Cela vous rapportera ${Math.floor(autoBrushAncPrice)} d'affection.`);
		document.getElementById('auto-brush-sell').style.borderColor = '#ffc107';
		document.getElementById('auto-brush-sell').style.color = '#ffc107';
		document.getElementById('auto-brush-sell').onmouseover = function() {
			document.getElementById('auto-brush-sell').style.backgroundColor = '#ffc107';
			document.getElementById('auto-brush-sell').style.color = '#212529';
		};
		document.getElementById('auto-brush-sell').onmouseout = function() {
			document.getElementById('auto-brush-sell').style.backgroundColor = 'black';
			document.getElementById('auto-brush-sell').style.color = '#ffc107';
		};
		aPS += autoBrushEff;
		document.getElementById('aPS').innerHTML = `Affection Par Seconde : ${Math.round(aPS*100)/100}`;
		$('#auto-brush-sell').show();
		$('#auto-brush-num').show();
		autoBrushIntervals.push(window.setInterval(autoBrushGen, autoBrushSpeed));
	}
}

function cook() {
	if (love >= cookPrice) {
		love -= cookPrice;
		document.getElementById('love').innerHTML = `Vous avez ${Math.floor(love)} d'affection`;
		cookAncPrice = cookPrice / 2;
		cookPrice *= 1.7;
		document.getElementById('cook').setAttribute('title', `Cela vous coûtera ${Math.floor(cookPrice)} d'affection. Cela rapporte ${Math.round(cookEff*100)/100} d'affection par seconde.`);
		cookNum++;
		document.getElementById('cook-num').innerHTML = `Vous disposez de ${cookNum} Cuisiniers.`;
		document.getElementById('cook-sell').setAttribute('title', `Cela vous rapportera ${Math.floor(cookAncPrice)} d'affection.`);
		document.getElementById('cook-sell').style.borderColor = '#ffc107';
		document.getElementById('cook-sell').style.color = '#ffc107';
		document.getElementById('cook-sell').onmouseover = function() {
			document.getElementById('cook-sell').style.backgroundColor = '#ffc107';
			document.getElementById('cook-sell').style.color = '#212529';
		};
		document.getElementById('cook-sell').onmouseout = function() {
			document.getElementById('cook-sell').style.backgroundColor = 'black';
			document.getElementById('cook-sell').style.color = '#ffc107';
		};
		aPS += cookEff;
		document.getElementById('aPS').innerHTML = `Affection Par Seconde : ${Math.round(aPS*100)/100}`;
		$('#cook-num').show();
		$('#cook-sell').show();
		cookIntervals.push(window.setInterval(cookGen, cookSpeed));
	}
}
// End of Buy Functions

// Sell Functions
function distribSell() {
	if (distribNum > 0) {
		love += distribAncPrice;
		document.getElementById('love').innerHTML = `Vous avez ${Math.floor(love)} d'affection`;
		distribPrice /= 1.5;
		distribAncPrice = distribPrice / 2;
		document.getElementById('distrib-sell').setAttribute('title', `Cela vous rapportera ${Math.floor(distribAncPrice)} d'affection.`);
		document.getElementById('distrib').setAttribute('title', `Cela vous coûtera ${Math.floor(distribPrice)} d'affection. Cela rapporte ${Math.round(distribEff*100)/100} d'affection par seconde.`);
		aPS -= distribEff;
		document.getElementById('aPS').innerHTML = `Affection Par Seconde : ${Math.round(aPS*100)/100}`;
		distribNum--;
		document.getElementById('distrib-num').innerHTML = `Vous disposez de ${distribNum} Distributeurs Automatique.`;
		if (distribNum <= 0) {
			document.getElementById('distrib-sell').style.borderColor = 'grey';
			document.getElementById('distrib-sell').style.color = 'grey';
			document.getElementById('distrib-sell').onmouseover = function() {
				document.getElementById('distrib-sell').style.backgroundColor = 'black';
			};
			document.getElementById('distrib-sell').onmouseout = function() {
				document.getElementById('distrib-sell').style.backgroundColor = 'black';
			};
		}
		window.clearInterval(distribIntervals.pop());
	}
}

function autoBrushSell() {
	if (autoBrushNum > 0) {
		love += autoBrushAncPrice;
		document.getElementById('love').innerHTML = `Vous avez ${Math.floor(love)} d'affection`;
		autoBrushPrice /= 1.5;
		autoBrushAncPrice = autoBrushPrice / 2;
		document.getElementById('auto-brush-sell').setAttribute('title', `Cela vous rapportera ${Math.floor(autoBrushAncPrice)} d'affection.`);
		document.getElementById('auto-brush').setAttribute('title', `Cela vous coûtera ${Math.floor(autoBrushPrice)} d'affection. Cela rapporte ${Math.round(autoBrushEff*100)/100} d'affection par seconde.`);
		aPS -= autoBrushEff;
		document.getElementById('aPS').innerHTML = `Affection Par Seconde : ${Math.round(aPS*100)/100}`;
		autoBrushNum--;
		document.getElementById('auto-brush-num').innerHTML = `Vous disposez de ${autoBrushNum} Brosses Automatique.`;
		if (autoBrushNum <= 0) {
			document.getElementById('auto-brush-sell').style.borderColor = 'grey';
			document.getElementById('auto-brush-sell').style.color = 'grey';
			document.getElementById('auto-brush-sell').onmouseover = function() {
				document.getElementById('auto-brush-sell').style.backgroundColor = 'black';
			};
			document.getElementById('auto-brush-sell').onmouseout = function() {
				document.getElementById('auto-brush-sell').style.backgroundColor = 'black';
			};
		}
		window.clearInterval(autoBrushIntervals.pop());
	}
}

function cookSell() {
	if (cookNum > 0) {
		love += cookAncPrice;
		document.getElementById('love').innerHTML = `Vous avez ${Math.floor(love)} d'affection`;
		cookPrice /= 1.5;
		cookAncPrice = cookPrice / 2;
		document.getElementById('cook-sell').setAttribute('title', `Cela vous rapportera ${Math.floor(cookAncPrice)} d'affection.`);
		document.getElementById('cook').setAttribute('title', `Cela vous coûtera ${Math.floor(cookPrice)} d'affection. Cela rapporte ${Math.round(cookEff*100)/100} d'affection par seconde.`);
		aPS -= cookEff;
		document.getElementById('aPS').innerHTML = `Affection Par Seconde : ${Math.round(aPS*100)/100}`;
		cookNum--;
		document.getElementById('cook-num').innerHTML = `Vous disposez de ${cookNum} Cuisiniers.`;
		if (cookNum <= 0) {
			document.getElementById('cook-sell').style.borderColor = 'grey';
			document.getElementById('cook-sell').style.color = 'grey';
			document.getElementById('cook-sell').onmouseover = function() {
				document.getElementById('cook-sell').style.backgroundColor = 'black';
			};
			document.getElementById('cook-sell').onmouseout = function() {
				document.getElementById('cook-sell').style.backgroundColor = 'black';
			};
		}
	}
	window.clearInterval(cookIntervals.pop());
}

// End of Sell Functions

// Upgrade Functions
function upg1() {
	if (love >= 500) {
		love -= 500;
		document.getElementById('love').innerHTML = `Vous avez ${Math.floor(love)} d'affection`;
		aPS -= (distribNum * distribEff);
		distribSpeed -= (distribSpeed * 20) / 100;
		distribEff += (distribEff * 20) / 100;
		document.getElementById('distrib').setAttribute('title', `Cela vous coûtera ${Math.floor(distribPrice)} d'affection. Cela rapporte ${Math.round(distribEff*100)/100} d'affection par seconde.`);
		aPS += (distribNum * distribEff);
		document.getElementById('aPS').innerHTML = `Affection Par Seconde : ${Math.round(aPS*100)/100}`;
		for (i = 0; i < distribNum; i++) {
			window.clearInterval(distribIntervals.pop());
		}
		distribIntervals = [];
		for (i = 0; i < distribNum; i++) {
			distribIntervals.push(window.setInterval(distribGen, distribSpeed));
		}
		$('#upg1').hide();
	}
}

function upg2() {
	if (love >= 1000) {
		love -= 1000;
		document.getElementById('love').innerHTML = `Vous avez ${Math.floor(love)} d'affection`;
		aPS -= (autoBrushNum * autoBrushEff);
		autoBrushSpeed -= (autoBrushSpeed * 20) / 100;
		autoBrushEff += (autoBrushEff * 20) / 100;
		document.getElementById('auto-brush').setAttribute('title', `Cela vous coûtera ${Math.floor(autoBrushPrice)} d'affection. Cela rapporte ${Math.round(autoBrushEff*100)/100} d'affection par seconde.`);
		aPS += (autoBrushNum * autoBrushEff);
		document.getElementById('aPS').innerHTML = `Affection Par Seconde : ${Math.round(aPS*100)/100}`;
		$('#upg2').hide();
		for (i = 0; i < autoBrushNum; i++) {
			window.clearInterval(autoBrushIntervals.pop());
		}
		autoBrushIntervals = [];
		for (i = 0; i < autoBrushNum; i++) {
			autoBrushIntervals.push(window.setInterval(autoBrushGen, autoBrushSpeed));
		}
	}
}

function upg3() {
	if (love >= 5000) {
		love -= 5000;
		document.getElementById('love').innerHTML = `Vous avez ${Math.floor(love)} d'affection`;
		aPS -= (cookNum * cookEff);
		cookSpeed -= (cookSpeed * 20) / 100;
		cookEff += (cookEff * 20) / 100;
		document.getElementById('cook').setAttribute('title', `Cela vous coûtera ${Math.floor(cookPrice)} d'affection. Cela rapporte ${Math.round(cookEff*100)/100} d'affection par seconde.`);
		aPS += (cookNum * cookEff);
		document.getElementById('aPS').innerHTML = `Affection Par Seconde : ${Math.round(aPS*100)/100}`;
		$('#upg3').hide();
		for (i = 0; i < cookNum; i++) {
			window.clearInterval(cookIntervals.pop());
		}
		cookIntervals = [];
		for (i = 0; i < cookNum; i++) {
			cookIntervals.push(window.setInterval(cookGen, cookSpeed));
		}
	}
}
// End of Upgrade Functions

// End of Pet Functions

// Moving Function
function move(whichB) {
	switch (position) {
		case 1:
		switch (whichB) {
			case 2:
			$('#workoutPlaces').hide();
			$('#magasin').show();
			position = 2;
			break;

			case 3:
			$('#workoutPlaces').hide();
			$('#arene').show();
			position = 3;
			break;

			case 4:
			$('#workoutPlaces').hide();
			switch(schoolPosition) {
				case 0:
				$('#school').show();
				$('#school0').show();
				break;

				case 1:
				$('#school').show();
				$('#school1').show();
				break;

				case 2:
				$('#school').show();
				$('#school2').show();
				break;

				case 3:
				$('#school').show();
				$('#school3').show();
				break;
			}
			position = 4;
			break;

			case 5:
			$('#workoutPlaces').hide();
			$('#politic').show();
			position = 5;
			break;

			case 6:
			$('#workoutPlaces').hide();
			petLaunch(petPosition);
			position = 6;
			break;
		}
		break;

		case 2:
		switch (whichB) {
			case 1:
			$('#magasin').hide();
			$('#workoutPlaces').show();
			position = 1;
			break;

			case 3:
			$('#magasin').hide();
			$('#arene').show();
			position = 3;
			break;

			case 4:
			$('#magasin').hide();
			switch(schoolPosition) {
				case 0:
				$('#school').show();
				$('#school0').show();
				break;

				case 1:
				$('#school').show();
				$('#school1').show();
				break;

				case 2:
				$('#school').show();
				$('#school2').show();
				break;

				case 3:
				$('#school').show();
				$('#school3').show();
				break;
			}
			position = 4;
			break;

			case 5:
			$('#magasin').hide();
			$('#politic').show();
			position = 5;
			break;

			case 6:
			$('#magasin').hide();
			petLaunch(petPosition);
			position = 6;
			break;
		}
		break;

		case 3:
		switch (whichB) {
			case 1:
			$('#arene').hide();
			$('#workoutPlaces').show();
			position = 1;
			break;

			case 2:
			$('#arene').hide();
			$('#magasin').show();
			position = 2;
			break;

			case 4:
			$('#arene').hide();
			switch(schoolPosition) {
				case 0:
				$('#school').show();
				$('#school0').show();
				break;

				case 1:
				$('#school').show();
				$('#school1').show();
				break;

				case 2:
				$('#school').show();
				$('#school2').show();
				break;

				case 3:
				$('#school').show();
				$('#school3').show();
				break;
			}
			position = 4;
			break;

			case 5:
			$('#arene').hide();
			$('#politic').show();
			position = 5;
			break;

			case 6:
			$('#arene').hide();
			petLaunch(petPosition);
			position = 6;
			break;
		}
		break;

		case 4:
		switch (whichB) {
			case 1:
			$('#school').hide();
			$('#workoutPlaces').show();
			position = 1;
			break;

			case 2:
			$('#school').hide();
			$('#magasin').show();
			position = 2;
			break;

			case 3:
			$('#school').hide();
			$('#arene').show();
			position = 3;
			break;

			case 5:
			$('#school').hide();
			$('#politic').show();
			position = 5;
			break;

			case 6:
			$('#school').hide();
			petLaunch(petPosition);
			position = 6;
			break;
		}
		break;

		case 5:
		switch (whichB) {
			case 1:
			$('#politic').hide();
			$('#workoutPlaces').show();
			position = 1;
			break;

			case 2:
			$('#politic').hide();
			$('#magasin').show();
			position = 2;
			break;

			case 3:
			$('#politic').hide();
			$('#arene').show();
			position = 3;
			break;

			case 4:
			$('#politic').hide();
			switch(schoolPosition) {
				case 0:
				$('#school').show();
				$('#school0').show();
				break;

				case 1:
				$('#school').show();
				$('#school1').show();
				break;

				case 2:
				$('#school').show();
				$('#school2').show();
				break;

				case 3:
				$('#school').show();
				$('#school3').show();
				break;
			}
			position = 4;
			break;

			case 6:
			$('#politic').hide();
			petLaunch(petPosition);
			position = 6;
			break;
		}
		break;

		case 6:
		switch (whichB) {
			case 1:
			$('#pet').hide();
			$('#workoutPlaces').show();
			position = 1;
			break;

			case 2:
			$('#pet').hide();
			$('#magasin').show();
			position = 2;
			break;

			case 3:
			$('#pet').hide();
			$('#arene').show();
			position = 3;
			break;

			case 4:
			$('#pet').hide();
			switch(schoolPosition) {
				case 0:
				$('#school').show();
				$('#school0').show();
				break;

				case 1:
				$('#school').show();
				$('#school1').show();
				break;

				case 2:
				$('#school').show();
				$('#school2').show();
				break;

				case 3:
				$('#school').show();
				$('#school3').show();
				break;
			}
			position = 4;
			break;

			case 5:
			$('#pet').hide();
			$('#politic').show();
			position = 5;
			break;
		}
		break;
	}
}
// End of Moving Function

// Event Listeners
let returnButtons = document.getElementsByClassName('return-button');
for (i = 0; i < returnButtons.length; i++) {
	returnButtons[i].addEventListener('click', function(){schoolBack(schoolPosition)});
}
document.getElementById('upg3').addEventListener('click', upg3);
document.getElementById('upg2').addEventListener('click', upg2);
document.getElementById('upg1').addEventListener('click', upg1);
document.getElementById('cook-sell').addEventListener('click', cookSell);
document.getElementById('auto-brush-sell').addEventListener('click', autoBrushSell);
document.getElementById('distrib-sell').addEventListener('click', distribSell);
document.getElementById('cook').addEventListener('click', cook);
document.getElementById('auto-brush').addEventListener('click', autoBrush);
document.getElementById('distrib').addEventListener('click', distrib);
document.getElementById('feed').addEventListener('click', feed);
document.getElementById('dogBuy').addEventListener('click', function(){animalChoice(2)});
document.getElementById('catBuy').addEventListener('click', function(){animalChoice(1)});
document.getElementById('petBuy').addEventListener('click', petBuy);
document.getElementById('politicButton').addEventListener('click', campagne);
document.getElementById('goCourse4').addEventListener('click', goCourse4Func);
document.getElementById('goCourse3').addEventListener('click', goCourse3Func);
document.getElementById('goCourse2').addEventListener('click', goCourse2Func);
document.getElementById('goCourse1').addEventListener('click', goCourse1Func);
document.getElementById('siCourse4').addEventListener('click', siCourse4Func);
document.getElementById('siCourse3').addEventListener('click', siCourse3Func);
document.getElementById('siCourse2').addEventListener('click', siCourse2Func);
document.getElementById('siCourse1').addEventListener('click', siCourse1Func);
document.getElementById('brCourse4').addEventListener('click', brCourse4Func);
document.getElementById('brCourse3').addEventListener('click', brCourse3Func);
document.getElementById('brCourse2').addEventListener('click', brCourse2Func);
document.getElementById('brCourse1').addEventListener('click', brCourse1Func);
document.getElementById('goldButton').addEventListener('click', gold);
document.getElementById('silverButton').addEventListener('click', silver);
document.getElementById('bronzeButton').addEventListener('click', bronze);
document.getElementById('areneButton').addEventListener('click', function(){arena(gladLvl, arenaOn)})
document.getElementById('move1').addEventListener('click', function(){move(1)});
document.getElementById('move2').addEventListener('click', function(){move(2)});
document.getElementById('move3').addEventListener('click', function(){move(3)});
document.getElementById('move4').addEventListener('click', function(){move(4)});
document.getElementById('move5').addEventListener('click', function(){move(5)});
document.getElementById('move6').addEventListener('click', function(){move(6)});
document.getElementById('chaMaxButton').addEventListener('click', cosmetic);
document.getElementById('intMaxButton').addEventListener('click', book);
document.getElementById('strMaxButton').addEventListener('click', coffee);
document.getElementById('theaButton').addEventListener('click', theatre);
document.getElementById('libButton').addEventListener('click', library);
document.getElementById('gymButton').addEventListener('click', gym);
document.getElementById('work').addEventListener('click', work);
document.getElementById('work2').addEventListener('click', work);
window.addEventListener('load', launch)
